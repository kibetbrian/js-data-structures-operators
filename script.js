'use strict';

const restaurant = {
    name: 'Classico Italiano',
    location: 'Kahawa Wendani, Ruiru, Kenya',
    categories: ['Kenyan', 'Pizza', 'Vegeterian', 'Organic'],
    starterMenu: ['Foccasia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
    mainMenu: ['Pizza', 'Pasta', 'Rissoto'],

    order: function(starterIndex, mainIndex) {
        return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
    }
};

const arr = [2, 3, 4];

const [a, b, c] = arr;
console.log(a, b, c);

let [first, , second] = restaurant.categories;
console.log(first, second);

//Switching Variables

[first, second] = [second, first];
console.log(first, second);

//Receive 2 Return values from a function

//console.log(restaurant.order(3, 2));
const [starterCourse, mainCourse] = restaurant.order(3, 2);
console.log(starterCourse, mainCourse);

//Nested Destructuring
const nested = [2, 3, [4, 5]];
// const [i, , j] = nested;
// console.log(i, j);
const [i, , [j, k]] = nested;
console.log(i, j, k);

//Default values